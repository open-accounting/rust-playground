# Basic Rust + GRPC example

Based on https://github.com/hyperium/tonic/blob/master/examples/helloworld-tutorial.md

- Client not yet implemented

## Usage

### Run the server

    $ cargo run --bin rust-grpc-server

### Test using grpcurl (make sure grpcurl is installed)

    $ grpcurl -plaintext -import-path ./proto -proto helloworld.proto -d '{"name": "Tonic"}' [::]:50052 helloworld.Greeter/SayHello

### Run the client

    $ cargo run --bin rust-grpc-client
